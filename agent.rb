# -*- coding: utf-8 -*-
=begin
2016/06/28
game.rb

20✕20マスの盤面

・歩36個
・体力2、攻撃力1、反撃はなし
・移動距離はマンハッタン距離3
・移動しなくても良い
・移動からの攻撃可能

=end
require 'paint'

#列挙型として扱う
#モジュール名::変数名　として使用 p Type::NUM #=> 0
module Type
  NUM = 0
  HP  = 1
  MOVED = 2
end

#移動力
Max_move = 3 
#コマンド長
Com_N = 3

#-----------------------------------------------------------------------------

class Hohei
  def initialize(team,num)
    @hp = 2      #ヒットポイント
    @attack = 1  #攻撃力
    @team = team #チーム判別  team = 1 or team = 2
    @num  = num  #コマ番号 動かすコマを選択するときに用いる
    @moved = 0   #すでに動かしたかどうかを記録　( 0 = 動かしていない 1 = すでに動かしている)
    @pos         #自分の位置(0〜399)を記録 
  end    

  def pos_set(pos)
    @pos = pos
  end

  def hp_upd
    @hp -= 1
  end

  def ret_pos
    return @pos
  end

  def ret_team
    return @team
  end

  def ret_num
    return @num
  end

  def ret_hp
    return @hp
  end

  def ret_moved
    return @moved
  end

end #Hohei

#------------------------------------------------------------------------------

class Board
  def initialize
    #board配列はコマのクラスを情報として持つ(空きマスはnull)
    @board = Array.new(400) #20✕20 400の配列
    @hu1_cla = Array.new #チーム1のコマ  要素は歩兵クラス
    @hu2_cla = Array.new #チーム2のコマ
    #コマの初期化
    for i in 1..36 do
      @hu1_cla.push(Hohei.new(1,i))
      @hu2_cla.push(Hohei.new(2,i))  
    end
    #盤面の初期化
    #空の盤面
    for i in 0..399 do
      @board[i] = 0
    end
    #コマがある盤面
    c1 = 0             #コマ配列の要素番号
    c2 = 0
    for n in 0..5 do
      #チーム1のコマ配置
      for i in 14..19 do
        p @board[i+20*n] = @hu1_cla[c1]
        @hu1_cla[c1].pos_set(i+20*n)
        c1 += 1
      end
      #チーム2のコマ配置
      for i in 280..285 do
        @board[i+20*n] = @hu2_cla[c2]
        @hu2_cla[c2].pos_set(i+20*n)
        c2 += 1
      end
    end
  end #initialize
  
  #盤面を表示する関数(引数によって表示を変更)
  def printf(type) 
    count = 0
    for i in 0..399 do
        if @board[i] == 0 then 
          #空きマスの表示
          print "\e[30m\e[47m - \e[0m"   #30:文字黒　47:背景白　0:リセット
        else                             
          #コマの表示　チーム1:赤字 31　チーム2:青字 34
          team = @board[i].ret_team
          if type == Type::NUM then      #コマ番号の表示
            
            a = @board[i].ret_num
            if a > 9  && team == 1 then
              print "\e[31m\e[47m#{a} \e[0m"
            elsif a > 9 && team == 2 then
              print "\e[34m\e[47m#{a} \e[0m"
            elsif team == 1 then
              print "\e[31m\e[47m0#{a} \e[0m"
            else  team == 2 
              print "\e[34m\e[47m0#{a} \e[0m"
            end

          elsif type == Type::HP then    #コマHPの表示

            if team == 1 then
              print "\e[31m\e[47m #{@board[i].ret_hp} \e[0m"
            else
              print "\e[34m\e[47m #{@board[i].ret_hp} \e[0m"
            end

          elsif type == Type::MOVED      #すでに動かしたコマは黒字で表示

            if @board[i].ret_moved == 1 then
              a = @board[i].ret_num
              if a < 10 then
                print "\e[30m\e[47m0#{a} \e[0m"
              else
                print "\e[30m\e[47m#{a} \e[0m"
              end
            else
              a = @board[i].ret_num
              if a > 9  && team == 1 then
                print "\e[31m\e[47m#{a} \e[0m"
              elsif a > 9 && team == 2 then
                print "\e[34m\e[47m#{a} \e[0m"
              elsif team == 1 then
                print "\e[31m\e[47m0#{a} \e[0m"
              else  team == 2 
                print "\e[34m\e[47m0#{a} \e[0m"
              end
            end

          end#if type
        end#if @board
      count += 1
      if count == 20 then
        puts ""
        count = 0
      end
    end#for i 
  end

  def ret_board
    return @board.dup
  end

  def ret_pos(num,team) #コマ番号numを受け取りそのコマの位置情報を返す
    if team == 1 then 
      return @hu1_cla[num].ret_pos
    elsif team == 2 then 
      return @hu2_cla[num].ret_pos
    end
  end

  def ret_judge
    c1=0
    c2=0
    @hu1_cla.each do |a|
      if a != 0 then
        c1 += 1
        break
      end
    end

    @hu2_cla.each do |a| 
      if a!=0 then
        c2 += 1
        break
      end
    end
    
    if c1 > 0 && c2 > 0 then
      return 0
    elsif c1 == 0 then
      return 2
    elsif c2 == 0 then
      return 1
    end
  end
  
  #actionコマンドを受け取り盤面を更新する
  def update_act(str,i,team)
   num = i-1 #コマ番号
   arr = Array.new
   arr = str.scan(/.{1,#{Com_N}}/)
   m1 = arr[0].to_i
   m2 = arr[1].to_i
   atk = arr[2].to_i
   if m1 != m2 then
     @board[m1] = 0
     if team == 1 then
       @board[m2] = @hu1_cla[num]
       @hu1_cla[num].pos_set(m2)
     else
       @board[m2] = @hu2_cla[num]
       @hu2_cla[num].pos_set(m2)
     end
   elsif m1 != atk then
       @board[atk].hp_upd
   end   
   printf(Type::NUM)
   sleep 1.5
  end

end #Board

#-------------------------------------------------------------------------------------  

class Game
  def initialize 
    @ban_class = Board.new
  end

  def show(type)
    @ban_class.printf(type)
  end

  #勝敗判定_boardクラスのret_judgeから値を受け取り判定
  def judge
    ret = @ban_class.ret_judge
    if ret != 0 then
      puts "team#{ret}の勝利！！"
      return 1
    else
      return ret
    end
  end

  def show_func
    puts"盤面表示変更,下の[]コマンドを入力"
    puts"[n]:コマ番号　[h]:残りHP　[m]:動かしたコマ番号"
    command = gets.chomp
    if command == 'n' then
      show(Type::NUM)
    elsif command == 'h' then
      show(Type::HP)
    elsif command == 'm' then
      show(Type::MOVED)
    else
      puts "##################################"
      puts "#正しいコマンドを入力してください#"
      puts "##################################"
    end
  end
  
  def move_attack(team)
    act = Action.new
    act_str = ""
    puts "player#{team}:コマを選択(1〜36)"
    puts "盤面を表示したい場合'b'を入力"
    for i in 1..36 do
      #command = gets.chomp #手入力
      command = 37-i #コマの番号
      str = ""
      if command == 'b' then
        show_func
      elsif 0 < command.to_i || command.to_i < 37 then 
        num = command.to_i - 1
        pos = @ban_class.ret_pos(num,team)
      end
      #選択したコマが存在する場合
      if pos >= 0 then
        r = MoveRange.new(@ban_class.ret_board,team,pos)
        range = r.ret_pos
        #移動決定
        if range.size != 0 then
           pos_i = rand(range.size+1)
           pos_g = range[pos_i]
           if pos_i == range.size then
           pos_g = pos
           end
           str += act.move(pos,pos_g)
        else
           if pos < 10 then
             str += "00#{pos}00#{pos}"
           elsif pos < 100 then
             str += "0#{pos}0#{pos}"
           else
             str += pos.to_s + pos.to_s
           end
        end
        #攻撃決定
        atk_pos = act.attack(pos_g,@ban_class.ret_board,team)
        if atk_pos < 10 then
            str += "00#{atk_pos}"
        elsif atk_pos < 100 then
            str += "0#{atk_pos}"
        else 
            str += "#{atk_pos}"
        end
        p act_str += str
        @ban_class.update_act(str,command,team)
      end
    end
    p act_str += "999"
  end
    
  def select(team)
    show(Type::NUM)
    while 1 do
      puts"player1(赤)　player2(青)"
      puts"player#{team}のターン"
      puts"コマンド選択"
      print"[s]移動コマ選択"
      print" [b]盤面表示変更"
      puts" [q]ターン終了"
      command = gets.chomp
      if command == 's' then #コマ選択モード 
        move_attack(team)
      elsif command == 'b'
        show_func
      elsif command == 'q'
        break
      else
        puts "##################################"
        puts "#正しいコマンドを入力してください#"
        puts "##################################"
      end
    end # while
  end

end #Game

#-------------------------------------------------------------------------------------
class Strategy
  
end
#-------------------------------------------------------------------------------------
class Action
  def serch(board) 
    if loop == 0 then
      pos1 = koma.ret_pos 
      arr = Array.new
    end
  end

  def move(pos_s,pos_g) #コマの初期位置とそのコマの目的地を引数として移動コマンドを返す
    if pos_s < 10 then
      str1 = "00#{pos_s}"
    elsif pos_s <100 then
      str1 = "0#{pos_s}"
    else 
      str1 = "#{pos_s}"
    end

    if pos_g < 10 then
      str2 = "00#{pos_g}"
    elsif pos_g < 100 then
      str2 = "0#{pos_g}"
    else
      str2 = "#{pos_g}"
    end

    str3 = str1 + str2

    return str3
  end
  
  def attack(pos_g,board,team)
    if team == 1 then
      op = 2
    elsif team == 2 then
      op = 1
    end
    up = board[pos_g-20]
    bot = board[pos_g+20]
    left = board[pos_g-1]
    right = board[pos_g+1]
    if up != 0 && up.ret_team == op then
      return up
    elsif bot != 0 && bot.ret_team == op then
      return bot
    elsif left != 0 && left.ret_team == op then
      return left
    elsif right != 0 && right.ret_team == op then
      return  right
    else 
      return pos_g
    end
  end
  
end
#--------------------------------------------------------------------------------------

class MoveRange #board:盤面情報 team:どっちのチームであるかを判別 pos:コマの位置(探索開始地点)
  def initialize(board,team,pos)
    @map = Array.new(400) #盤面の移動消費量を記録(空、味方のコマあり:-1、敵のコマあり:-9(移動不可))
    @move = Array.new(400)#コマが移動可能な場所であるかを記録(移動可能なら0以上の値が記録される)
    @ban = board
    @p = pos
    for i in 0..399 do
      @move[i] = -1
      if board[i] == 0 then
        @map[i] = -1
      else
        t = board[i].ret_team
        if t == team then
          @map[i] = -1
        else
          @map[i] = (Max_move+1)*(-1)
        end
      end
    end
  end
=begin
  #デバック用
  def printf 
    count = 0
    for i in 0..399 do
      count += 1
      if @move[i] < 0 then
        print "#{@move[i]}"
      else
        print " #{@move[i]}"
      end
      
      if count == 20 then
        puts ""
        count = 0
      end
    end
  end
=end

  def search(pos,mp)
    mp = mp + @map[pos];
    if mp > @move[pos] then
      @move[pos] = mp
    end
    
    if mp > 0 then
      search4(pos,mp)
    end
    
  end
  
  def search4(pos,mp)
    #up
    pnext = pos - 20
    if pnext > -1 then
      search(pnext,mp)
    end
    #down
    pnext = pos + 20
    if pnext < 400 then
      search(pnext,mp)
    end
    #left
    if pos!=0 || (pos%20)!=0 then
      pnext = pos - 1
      search(pnext,mp)
    end
    #right 
    pnext = pos + 1
    if (pnext%20) != 0 then
      search(pnext,mp)
    end
  end  

  #移動可能場所の位置が記録された配列を返す関数
  def ret_pos
    pos_arr = Array.new
    search4(@p,Max_move) #探索
    for i in 0..399 do
      #到達範囲かつ空き盤面のとき
      if @move[i] > -1 && @ban[i] == 0 then
        pos_arr.push(i)
      end
    end
    return pos_arr
  end

end #Tansku

#-------------------------------------------------------------------------------------------------------

def main
  game = Game.new
  while game.judge == 0 do
    game.select(1)
    if game.judge != 0 then
      break
    end 
    game.select(2)
  end
end

main
