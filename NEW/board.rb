# -*- coding: utf-8 -*-
=begin
2016_10_29
サーバ対応
=end
module Type
  NUM = 0
  HP = 1
  MOVED = 2
end
#移動力
Max_move = 3
#コマンド長
Com_N = 3
#-------------------盤面クラス---------------------------
class Board
  def initialize
    #board配列はコマのクラスを情報として持つ(空きマスはnull)
    @board = Array.new(400) #20✕20
    @hu1_cla = Array.new #チーム1のコマ　要素は歩兵クラス
    @hu2_cla = Array.new #チーム2のコマ
    #コマの初期化
    for i in 1..36 do
      @hu1_cla.push(Hohei.new(1,i))
      @hu2_cla.push(Hohei.new(2,i))
    end
    #盤面の初期化
    #空の盤面
    for i in 0..399 do
      @board[i] = 0
    end
    #コマがある盤面
    c1 = 0
    c2 = 0
    for n in 0..5 do
      #チーム1のコマ配置
      for i in 14..19 do
        @board[i+20*n] = @hu1_cla[c1]
        @hu1_cla[c1].pos_set(i+20*n)
        c1 += 1
      end
      #チーム2のコマ配置
      for i in 280..285 do
        @board[i+20*n] = @hu2_cla[c2]
        @hu2_cla[c2].pos_set(i+20*n)
        c2+=1
      end
    end
  end#initialize
  #盤面表示 チーム1は赤、チーム2は青でコマのHPを表示する
  def printf
    count = 0
    for i in 0..399 do
      if @board[i] == 0 then
        #空きますの表示
        print "\e[30m\e[47m - \e[0m" #30:黒文字　47背景白　0:リセット
      else
        #コマの表示　チーム1：赤字31　チーム2：青字34
        team = @board[i].ret_team
        if team == 1 then
          print "\e[31m\e[47m #{@board[i].ret_hp} \e[0m"
        else
          print "\e[34m\e[47m #{@board[i].ret_hp} \e[0m"
        end
      end
      count += 1
      #20行ごとに改行
      if count == 20 then
        puts ""
        count = 0
      end
    end #for i in 0..399 do
  end
  #盤面配列を返す関数
  def ret_board
    return @board.dup
  end
  #コマ番号を受け取りそのコマの位置を返す
  #配列の要素がnilだったら-1を返す
  def ret_pos(num,team)
    if team == 1 then
      if @hu1_cla[num] == NIL then
        return -1
      else
        return @hu1_cla[num].ret_pos
      end
    elsif team==2 then
      if @hu2_cla[num] == NIL then
        return -1
      else
        return @hu2_cla[num].ret_pos
      end
    end
  end
  #勝敗判定
  def ret_judge
    c1=0
    c2=0
    @hu1_cla.each do |a|
      if a != NIL then
        c1 += 1
        break
      end
    end
    
    @hu2_cla.each do |a|
      if a!=NIL then
        c2 += 1
        break
      end
    end
    
    if c1 > 0 && c2 > 0 then
      return 0
    elsif c1 == 0 then
      return 2
    elsif c2 == 0 then
      return 1
    end
  end
  #actionコマンドを受け取り盤面を更新する
  def update_act(str,i,team)
    num = i - 1 #コマ番号
    arr = Array.new
    arr = str.scan(/.{1,#{Com_N}}/)
    m1 = arr[0].to_i
    m2 = arr[1].to_i
    atk = arr[2].to_i
    #現在地と移動先が異なる場合
    if m1 != m2 then
      @board[m1] = 0
      if team == 1 then
        @board[m2] = @hu1_cla[num]
        @hu1_cla[num].pos_set(m2)
        #puts "m2 = #{m2}"
        #puts "ret_pos1 = #{@hu1_cla[num].ret_pos}"
        #sleep 2.0
      else
        @board[m2] = @hu2_cla[num]
        @hu2_cla[num].pos_set(m2)
        #puts "m2 = #{m2}"
        #puts "ret_pos2 = #{@hu2_cla[num].ret_pos}"
        #sleep 2.0
      end
    elsif m1 == m2 then     #現在地と移動先が同じ時
      #puts "m1 = m2 = #{m2} "
      if team == 1 then
        #puts "ret_pos1 = #{@hu1_cla[num].ret_pos}"
      else
        #puts "ret_pos2 = #{@hu2_cla[num].ret_pos}"
      end
      #sleep 2.0
    end
    #攻撃するとき  
    if m2 != atk then
      @board[atk].hp_upd
      #攻撃されHPが0になったとき
      if @board[atk].ret_hp == 0 then
        rm = @board[atk].ret_num-1 #削除するべきコマ番号-1
        @board[atk] = 0
        if team == 1 then
          @hu2_cla[rm] = NIL
        elsif team == 2 then
          @hu1_cla[rm] = NIL
        end
      end
    elsif m2 == atk then  #攻撃しない時
      #puts "not attack"
    end
    #sleep 0.5   #sleepして経過確認 
  end
  #サーバーから受け取った文字列を解析し盤面情報を更新
  def update(str)
    #文字列を受け取り2文字ずつで区切る
    n = 2
    str_arr = str.scan(/.{1,#{n}}/)
    #コマ番号用の変数
    num_A = 1
    num_B = 1
    #歩兵クラス配列のリセット
    @hu1_cla.clear
    @hu2_cla.clear
    #盤面情報を更新
    #iが盤面番号に相当する
    for i in 0..399 do
      if str_arr[i] == "00" then
        @board[i] = 0
      else
        arr = str_arr[i].split("")
        if arr[0] == "A" then #先攻(1)の場合
          new = Hohei.new(1,num_A)
          new.pos_set(i)
          new.hp_ch(arr[1].to_i)
          @hu1_cla.push(new)
          @board[i] = @hu1_cla[num_A-1]
          num_A += 1
        elsif arr[0] == "B" then #後攻(2)の場合
          new = Hohei.new(2,num_B)
          new.pos_set(i)
          new.hp_ch(arr[1].to_i)
          @hu2_cla.push(new)
          @board[i] = @hu2_cla[num_B-1]
          num_B += 1
        end
      end
    end
    if num_A < 36 then
      for n in num_A..36 do
        @hu1_cla.push(NIL)
      end
    end
    if num_B < 36 then
      for n in num_B..36 do
        @hu2_cla.push(NIL)
      end
    end
    #経過確認
    printf
    sleep 1.0 
  end
end #Board_end
