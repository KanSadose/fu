# -*- coding: utf-8 -*-
=begin
2016_10_29
サーバ対応
=end
#'./'はカレントディレクトリを意味する
require './hohei.rb'
require './board.rb'
require './game.rb'
require './moverange.rb'
require './hohei.rb'
require './action.rb'
require "socket"

Client_Recv_Size = 20*3*20-1;
Port_num = ARGV[0].to_i

def main
  #192.168.11.15
  sock = TCPSocket.open("192.168.11.15", Port_num)
  game = Game.new

  #サーバから返却された文字列を出力
  board_str = sock.gets(Client_Recv_Size)
  game.board_upd(board_str)
  puts "recv : defaultMap"
  
  if Port_num == 20000 then
     player = 1
  elsif Port_num ==20001 then
     player = 2
  end
  
  while(1)
    act_command = ""
    if player == 2 then #後攻の時は受信してから送信
      #ソケットから文字列を受け取り盤面情報を変更
      board_str = sock.gets(Client_Recv_Size)
      game.board_upd(board_str)
      puts "recvA : end"
      act_command = game.start(player)
      #ソケットに入力文字列を渡す
      sock.puts act_command
      sock.flush
      puts "B : send end"
    elsif player == 1 then #先攻は送信してから受信
      act_command = game.start(player)
      #ソケットに入力文字列を渡す
      sock.puts act_command
      sock.flush
      puts "A : send end"
      #サーバから返却された文字列を受け取り盤面更新
      board_str = sock.gets(Client_Recv_Size)
      game.board_upd(board_str)
      puts "recvA : end"
    end
      #サーバから返却された文字列を受け取り盤面更新
      board_str = sock.gets(Client_Recv_Size)
      game.board_upd(board_str)
      puts "recvB : end"
      #無理やり繋ぎ直す
      sock.close
      #127.0.0.1
      sock = TCPSocket.open("192.168.11.15", Port_num)
  end
end

main
