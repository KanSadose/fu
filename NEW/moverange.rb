# coding: utf-8
class MoveRange #board:盤面情報 team:どっちのチームであるかを判別 pos:コマの位置(探索開始地点)
  def initialize(board,team,pos)
    @map = Array.new(400)
    #盤面の移動消費量を記録(空、味方のコマあり:-1、敵のコマあり:-9(移動不可))
    @move = Array.new(400)
    #コマが移動可能な場所であるかを記録(移動可能なら0以上の値が記録される)
    @ban = board
    @p = pos
    for i in 0..399 do
      @move[i] = -1
      if board[i] == 0 then
        @map[i] = -1
      else
        t = board[i].ret_team
        if t == team then
          @map[i] = -1
        else
          @map[i] = (Max_move+1)*(-1)
        end
      end
    end
  end
=begin
  #デバック用
  def printf 
    count = 0
    for i in 0..399 do
      count += 1
      if @move[i] < 0 then
        print "#{@move[i]}"
      else
        print " #{@move[i]}"
      end
      
      if count == 20 then
        puts ""
        count = 0
      end
    end
  end
=end

  def search(pos,mp)
    mp = mp + @map[pos];
    if mp > @move[pos] then
      @move[pos] = mp
    end
    
    if mp > 0 then
      search4(pos,mp)
    end
    
  end
  
  def search4(pos,mp)
    #up
    pnext = pos - 20
    if pnext > -1 then
      search(pnext,mp)
    end
    #down
    pnext = pos + 20
    if pnext < 400 then
      search(pnext,mp)
    end
    #left ここだけ条件文が少し違うがこれでOK
    if pos!=0 && (pos%20)!=0 then
      pnext = pos - 1
      search(pnext,mp)
    end
    #right 
    pnext = pos + 1
    if pnext != 400 && (pnext%20) != 0 then
      search(pnext,mp)
    end
  end  

  #移動可能場所の位置が記録された配列を返す関数
  def ret_pos_arr
    pos_arr = Array.new
    search4(@p,Max_move) #探索
    for i in 0..399 do
      #到達範囲かつ空き盤面のとき
      if @move[i] > -1 && @ban[i] == 0 then
        pos_arr.push(i)
      end
    end
    return pos_arr
  end

end 
