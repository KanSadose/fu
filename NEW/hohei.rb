# -*- coding: utf-8 -*-
=begin
2016_10_29
サーバ対応
=end

#--------歩兵クラス------------------------------------
class Hohei
  def initialize(team,num)
    @hp = 2
    @attack = 1  #攻撃力
    @team = team #チーム判別　1(先攻)or2(後攻)
    @num = num   #コマ番号 1-36
    @moved = 0   #すでに動かしたかを記録
    @pos         #自分の位置を記録
  end
  #自身の位置を記録
  def pos_set(pos)
    @pos = pos
  end
  #攻撃を受けた際のHP更新
  def hp_upd
    @hp -= 1
  end
  #HPの変更
  def hp_ch(n)
    @hp = n
  end
  #自身の位置を返す
  def ret_pos
    return @pos
  end
  #自身のチーム番号を返す
  def ret_team
    return @team
  end
  #自身のコマ番号を返す
  def ret_num
    return @num
  end
  #自身のHPを返す
  def ret_hp
    return @hp
  end
  #動いたかどうかの情報を返す
  def ret_moved
    return @moved
  end
end #Hohei_class
