# coding: utf-8
##### 兵ズのサーバープログラム #####
##### 原田 貴史 #####
##### 2016.8.6 #####

#!/usr/bin/ruby

require "socket"
Server_Recv_Size = 3*3*36+3;

#=============================================module=======================================
class Heizu_server
  #クラスの初期化
  def initialize
    @mapArray = Array.new(20){Array.new(20,"00")}
    @map = ""
    @command = Array.new(3*36+1,"000")
  end

  #マップの初期化
  def initialize_map
    for i in 0..19 do
      for j in 0..19 do
        if(14<=i and i<20 and 0<=j and j<=5) then
          @mapArray[i][j] = "A2"
        elsif(0<=i and i<=5 and 14<=j and j<=20) then
          @mapArray[i][j] = "B2"
        else
        end
      end
    end
  end

  #マップ確認用
  def print_map
    #puts @mapArray
    @map = ""
    for i in 0..19 do
      for j in 0..19 do
         @map = @map + @mapArray[i][j] + ":"
      end
      @map = @map + "\n"
    end
    puts @map
=begin
    count = 1
    @map.each do |a|
      print "#{a}"
      if count == 20 then
        puts ""
        count = 1
      end
      count += 1
    end
=end
  end

  #両クライアントへマップの送信
  def send_map(client_A, client_B)
    @map = ""
    for i in 0..19 do
      for j in 0..19 do
         @map = @map + @mapArray[i][j]
      end
    end
    # ruby用
    client_A.puts @map
    client_B.puts @map
    # C用
    #client_A.print @map
    #client_B.print @map
  end

  #Aのコマンドの実行
  def receive_command_A(commandA)
    comm = commandA.delete("\u0000")
    team = "A"
    @command = comm.scan(/.{1,9}/) #受信したコマンドを3文字ずつに分割
    #puts @command

    @command.each{|position|
      if((position.scan(/.{1,3}/))[0] == "999") then
        #puts "END"
        break;
      end
      command = position.scan(/.{1,3}/)

      defa_posi = @mapArray[command[0].to_i/ 20][command[0].to_i % 20]
      next_posi = @mapArray[command[1].to_i/ 20][command[1].to_i % 20]
      attc_posi = @mapArray[command[2].to_i/ 20][command[2].to_i % 20]
      #puts "each"
      #puts defa_posi
      #puts next_posi
      #puts attc_posi

      #選択した位置に自軍の兵がいるか確認
      if(/A/ =~ defa_posi) then
        #移動先に誰もいないか、元と同じ場所か確認
        #if(next_posi == "00" or defa_posi == next_posi) then
        if(self.can_move?( (command[0].to_i / 20), (command[0].to_i % 20), (command[1].to_i / 20), (command[1].to_i % 20), 3,team) == 1) then
          #puts "move"
          @mapArray[command[0].to_i/ 20][command[0].to_i % 20] = "00"
          @mapArray[command[1].to_i/ 20][command[1].to_i % 20] = defa_posi
          #攻撃先に相手がいるか確認
          #if(/B/ =~ attc_posi) then
          if( (self.can_attack?( (command[1].to_i/ 20), (command[1].to_i % 20), (command[2].to_i / 20), (command[2].to_i % 20)) == true) and (/B/ =~ attc_posi)) then
            #puts "attck"
            if(attc_posi[1].to_i == 2) then
              @mapArray[command[2].to_i/ 20][command[2].to_i % 20] = "B1"
            else
              @mapArray[command[2].to_i/ 20][command[2].to_i % 20] = "00"
            end 
          end
        end
      end
    }
  end

  #Bのコマンドの実行
  def receive_command_B(commandB)
    comm = commandB.delete("\u0000")
    team = "B"
    @command = comm.scan(/.{1,9}/) #受信したコマンドを9文字ずつ[一つの行動分]に分割
    #puts @command

    @command.each{|position|
      #999が入力されたら終了する
      if((position.scan(/.{1,3}/))[0] == "999") then
        #puts "END"
        break;
      end
      command = position.scan(/.{1,3}/) #一つの行動を位置座標[元位置、移動先、攻撃先]ごとに分割

      defa_posi = @mapArray[command[0].to_i/ 20][command[0].to_i % 20]
      next_posi = @mapArray[command[1].to_i/ 20][command[1].to_i % 20]
      attc_posi = @mapArray[command[2].to_i/ 20][command[2].to_i % 20]

      #選択した位置に自軍の兵がいるか確認
      if(/B/ =~ defa_posi) then
        #移動先に誰もいないか、元と同じ場所か確認
        #if(next_posi == "00" or defa_posi == next_posi) then
        if(self.can_move?( (command[0].to_i/ 20), (command[0].to_i % 20), (command[1].to_i / 20), (command[1].to_i % 20), 3,team) == 1) then
          #puts "move"
          @mapArray[command[0].to_i/ 20][command[0].to_i % 20] = "00"
          @mapArray[command[1].to_i/ 20][command[1].to_i % 20] = defa_posi
          #攻撃先に相手がいるか確認
          #if(/A/ =~ attc_posi) then
          if( (self.can_attack?( (command[1].to_i/ 20), (command[1].to_i % 20), (command[2].to_i / 20), (command[2].to_i % 20)) == true) and (/A/ =~ attc_posi)) then
            #puts "attck"
            if(attc_posi[1].to_i == 2) then
              @mapArray[command[2].to_i/ 20][command[2].to_i % 20] = "A1"
            else
              @mapArray[command[2].to_i/ 20][command[2].to_i % 20] = "00"
            end 
          end
        end
      end
    }
  end

#########################################################################################
#########################################################################################
#########################################################################################
  # 移動判定                       #初期で3で受け取る
  def can_move?( old_x, old_y, new_x, new_y, move_power,team)
    move_flag = 0
    #puts "ox:#{old_x},oy:#{old_y},nx:#{new_x},ny:#{new_y}"
    dx = old_x - new_x
    dy = old_y - new_y

    # 終了判定
    if( old_x == new_x and old_y == new_y) then
      return 1
    elsif(move_power == 0) then
      return 0
    else
      # 再起で回す
      #p "x:#{old_x+1}, y:#{old_y}"
      #p "#{@map[old_x+1][old_y]}"
      if(dx < 0) then
        if(@mapArray[old_x+1][old_y] == "00" || @mapArray[old_x+1][old_y].index(team) != NIL) then
          move_flag = self.can_move?( old_x+1, old_y, new_x, new_y, move_power-1,team)
        end
      elsif(dx > 0) then
        if(@mapArray[old_x-1][old_y] == "00" || @mapArray[old_x-1][old_y].index(team) != NIL) then
          move_flag = self.can_move?( old_x-1, old_y, new_x, new_y, move_power-1,team)
        end
      end

      if(move_flag == 1) then
        return 1
      end

      if(dy < 0) then
        if(@mapArray[old_x][old_y+1] == "00"|| @mapArray[old_x][old_y+1].index(team) != NIL) then
          move_flag = self.can_move?( old_x, old_y+1, new_x, new_y, move_power-1,team)
        end
      elsif(dy > 0) then
        if(@mapArray[old_x][old_y-1] == "00"|| @mapArray[old_x][old_y-1].index(team) != NIL) then
          move_flag = self.can_move?( old_x, old_y-1, new_x, new_y, move_power-1,team)
        end
      end
    end
    #puts move_flag
    return move_flag
  end

  # 攻撃判定
  def can_attack?( my_x, my_y, enemy_x, enemy_y)
    dx = (my_x - enemy_x).abs
    dy = (my_y - enemy_y).abs
    if(dx+dy == 1) then
      return true
    end
    return false
  end
#########################################################################################
#########################################################################################
#########################################################################################


  #終了判定
  def check_end
    if(@map.include?("A") == false or @map.include?("B") == false) then
      return true
    else
      return false
    end 
  end

end



def main
  # クライアントからの接続を待つソケット
  #サーバーのIP "192.168.11.5"
  sa = TCPServer.open(20000)
  sb = TCPServer.open(20001)
  commandA="";
  commandB="";

  #ヘイズのシステム
  server = Heizu_server.new

  #盤面の初期化
  server.initialize_map
  #server.print_map

  # クライアントからの接続をacceptする
  sock_A = sa.accept
  puts "[Accept_teamA]:OK"
  sock_B = sb.accept
  puts "[Accept_teamB]:OK"

  #初期盤面の送信
  server.send_map(sock_A,sock_B)
  puts "[Send Default_Map]:OK"


  # 終了するまで繰り返す
  while (server.check_end == false)
    if (server.check_end == false) then
      # クライアントから文字列を受信
      commandA = sock_A.gets
      # 行動を実行
      server.receive_command_A(commandA)

      #マップの送信
      server.send_map(sock_A,sock_B)
    end

    #B側からの攻撃
    if (server.check_end == false) then
      # クライアントから文字列を受信
      commandB = sock_B.gets
      # 行動を実行
      server.receive_command_B(commandB)

      #マップの送信
      server.send_map(sock_A,sock_B)
    end

    #ここで無理やりつなぎなおしている。
    sock_A.close
    sock_B.close
    #puts "A and B close"
    sock_A = sa.accept
    sock_B = sb.accept
    #puts "A and B connect reset"

  end

  # クライアントからの接続を待つソケットを閉じる
  sock_A.close
  sock_B.close
  puts "[Close]:OK!"
end

main
