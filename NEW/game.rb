# coding: utf-8
class Game
  def initialize
    @ban_class = Board.new
  end
  #サーバから受け取った文字列を解析し盤面情報を更新する
  def board_upd(str)
    puts "game.board_upd"
    @ban_class.update(str)
  end
  
  def show
    @ban_class.printf()
  end
  #勝敗判定_boardクラスのret_judgeから値を受け取り判定
  def judge
    ret = @ban_class.ret_judge
    if ret != 0 then
      puts "team#{ret}の勝利!!"
      return 1
    else
      return 0
    end
  end
  #行動決定
  def move_attack(team)
    act = Action.new
    act_str = ""
    for i in 1..36 do
      command = 37 - i #コマの番号
      str = ""
      if 0 < command.to_i || command.to_i < 37 then
        num = command.to_i - 1
        pos = @ban_class.ret_pos(num,team)
      end
      #選択したコマが存在する場合
      if pos >= 0 then
        r = MoveRange.new(@ban_class.ret_board,team,pos)
        range = r.ret_pos_arr #移動可能先の位置を記録
        #移動決定
        if range.size != 0 then
          pos_i = rand(range.size+1)
          pos_g = range[pos_i]
          if pos_i == range.size then
            pos_g = pos
          end
          str += act.move(pos,pos_g)
        else
          if pos < 10 then
            str += "00#{pos}00#{pos}"
          elsif pos < 100 then
            str += "0#{pos}0#{pos}"
          else
            str += pos.to_s + pos.to_s
          end
        end
        #攻撃決定
        atk_pos = act.attack(pos_g,@ban_class.ret_board,team)
        if atk_pos < 10 then
          str += "00#{atk_pos}"
        elsif atk_pos < 100 then
          str += "0#{atk_pos}"
        else
          str += "#{atk_pos}"
        end
        act_str += str
        #行動列によるコマンドを実行したのち盤面更新
        @ban_class.update_act(str,command,team)
      end
    end
    act_str += "999"
    show
    return  act_str
    #sleep 2.0
  end
  #ゲーム開始
  def start(team)
    return move_attack(team)
  end
  
end
