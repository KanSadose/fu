
class Tansaku
  def initialize
    @map = Array.new(100)
    @move = Array.new(100)
    for i in 0..99 do
      @map[i] = -1 
      @move[i] = -1
    end
    @map[34] = -9
  end

  def printf 
    count = 0
    for i in 0..99 do
      count += 1
      if @move[i] < 0 then
        print "#{@move[i]}"
      else
        print " #{@move[i]}"
      end

      if count == 10 then
        puts ""
        count = 0
      end
    end
  end

  def search(pos,mp)
    mp = mp + @map[pos];
    if mp > @move[pos] then
      @move[pos] = mp
    end

    if mp > 0 then
      search4(pos,mp)
    end
    printf
    puts ""
  end

  def search4(pos,mp)
    #up
    pnext = pos - 10
    if pnext > -1 then
      search(pnext,mp)
    end
    #down
    pnext = pos + 10
    if pnext < 100 then
      search(pnext,mp)
    end
    #left
    if pos!=0 || (pos%10)!=0 then
      pnext = pos - 1
      search(pnext,mp)
    end
    #right 
    pnext = pos + 1
    if (pnext%10) != 0 then
      search(pnext,mp)
    end
  end

end

def main
  t = Tansaku.new()
  t.search4(13,3)
  t.printf
end

main
