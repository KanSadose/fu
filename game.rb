# -*- coding: utf-8 -*-
=begin
2016/06/28
game.rb

20✕20マスの盤面

・歩36個
・体力2、攻撃力1、反撃はなし
・移動距離はマンハッタン距離3
・移動しなくても良い
・移動からの攻撃可能

=end
require 'paint'

#列挙型として扱う
#モジュール名::変数名　として使用 p Type::NUM #=> 0
module Type
  NUM = 0
  HP  = 1
  MOVED = 2
end


#-----------------------------------------------------------------------------

class Hohei
  def initialize(team,num)
    @hp = 2      #ヒットポイント
    @attack = 1  #攻撃力
    @team = team #チーム判別  team = 1 or team = 2
    @num  = num  #コマ番号 動かすコマを選択するときに用いる
    @moved = 0   #すでに動かしたかどうかを記録　( 0 = 動かしていない 1 = すでに動かしている)
    @pos         #自分の位置(0〜399)を記録 
  end    

  def pos_set(pos)
    @pos = pos
  end

  def ret_pos
    return @pos
  end

  def ret_team
    return @team
  end

  def ret_num
    return @num
  end

  def ret_hp
    return @hp
  end

  def ret_moved
    return @moved
  end

end #Hohei

#------------------------------------------------------------------------------

class Board
  def initialize
    #board配列はコマのクラスを情報として持つ(空きマスはnull)
    @board = Array.new(400) #20✕20 400の配列
    @hu1 = Array.new #チーム1のコマ
    @hu2 = Array.new #チーム2のコマ
    #コマの初期化
    for i in 1..36 do
      @hu1.push(Hohei.new(1,i))
      @hu2.push(Hohei.new(2,i))  
    end
    #盤面の初期化
    #空の盤面
    for i in 0..399 do
      @board[i] = 0
    end
    #コマがある盤面
    c1 = 0             #コマ配列の要素番号
    c2 = 0
    for n in 0..5 do
      #チーム1のコマ配置
      for i in 14..19 do
        @board[i+20*n] = @hu1[c1]
        @hu1[c1].pos_set(i+10*n)
        c1 += 1
      end
      #チーム2のコマ配置
      for i in 280..285 do
        @board[i+20*n] = @hu2[c2]
        @hu2[c2].pos_set(i+10*n)
        c2 += 1
      end
    end
  end #initialize
  
  #盤面を表示する関数(引数によって表示を変更)
  def printf(type)
    count = 0
    for i in 0..399 do
        if @board[i] == 0 then 
          #空きマスの表示
          print "\e[30m\e[47m - \e[0m"   #30:文字黒　47:背景白　0:リセット
        else                             
          #コマの表示　チーム1:赤字 31　チーム2:青字 34
          team = @board[i].ret_team
          if type == Type::NUM then      #コマ番号の表示
            
            a = @board[i].ret_num
            if a > 9  && team == 1 then
              print "\e[31m\e[47m#{a} \e[0m"
            elsif a > 9 && team == 2 then
              print "\e[34m\e[47m#{a} \e[0m"
            elsif team == 1 then
              print "\e[31m\e[47m0#{a} \e[0m"
            else  team == 2 
              print "\e[34m\e[47m0#{a} \e[0m"
            end

          elsif type == Type::HP then    #コマHPの表示

            if team == 1 then
              print "\e[31m\e[47m #{@board[i].ret_hp} \e[0m"
            else
              print "\e[34m\e[47m #{@board[i].ret_hp} \e[0m"
            end

          elsif type == Type::MOVED      #すでに動かしたコマは黒字で表示

            if @board[i].ret_moved == 1 then
              a = @board[i].ret_num
              if a < 10 then
                print "\e[30m\e[47m0#{a} \e[0m"
              else
                print "\e[30m\e[47m#{a} \e[0m"
              end
            else
              a = @board[i].ret_num
              if a > 9  && team == 1 then
                print "\e[31m\e[47m#{a} \e[0m"
              elsif a > 9 && team == 2 then
                print "\e[34m\e[47m#{a} \e[0m"
              elsif team == 1 then
                print "\e[31m\e[47m0#{a} \e[0m"
              else  team == 2 
                print "\e[34m\e[47m0#{a} \e[0m"
              end
            end

          end#if type
        end#if @board
      count += 1
      if count == 20 then
        puts ""
        count = 0
      end
    end#for i 
  end

  #勝敗判定＿hu1の要素0ならチーム2の勝利(2を返す)
  def ret_judge
    if @hu1.size == 0 then
      return 2
    elsif @hu2.size == 0 then
      return 1
    else
      return 0
    end
  end

end #Board

#-------------------------------------------------------------------------------------  

class Game
  def initialize 
    @board = Board.new
  end

  def show(type)
    @board.printf(type)
  end

  #勝敗判定_boardクラスのret_judgeから値を受け取り判定
  def judge
    ret = @board.ret_judge
    if ret != 0 then
      puts "team#{ret}の勝利！！"
      return 1
    else
      return ret
    end
  end

  def show_func
    puts"盤面表示変更,下の[]コマンドを入力"
    puts"[n]:コマ番号　[h]:残りHP　[m]:動かしたコマ番号"
    command = gets.chomp
    if command == 'n' then
      show(Type::NUM)
    elsif command == 'h' then
      show(Type::HP)
    elsif command == 'm' then
      show(Type::MOVED)
    else
      puts "##################################"
      puts "#正しいコマンドを入力してください#"
      puts "##################################"
    end
  end
  
  def select(team)
    show(Type::NUM)
    while 1 do
      puts"player1(赤)　player2(青)"
      puts"player#{team}のターン"
      puts"コマンド選択"
      print"[s]移動コマ選択"
      print" [b]盤面表示変更"
      puts" [q]ターン終了"
      command = gets.chomp
      if command == 's' then #コマ選択モード 
        # move_attack
      elsif command == 'b'
        show_func
      elsif command == 'q'
        break
      else
        puts "##################################"
        puts "#正しいコマンドを入力してください#"
        puts "##################################"
      end
    end # while
  end

end #Game

#--------------------------------------------------------------------------------------

def main
  game = Game.new
  while game.judge == 0 do
    game.select(1)
    if game.judge != 0 then
      break
    end 
    game.select(2)
  end
end

main
